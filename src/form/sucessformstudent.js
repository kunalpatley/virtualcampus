import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import Newnav from "../component/newnav";

import female from "../images/Female.png";
import male from "../images/Male.png";
import Student from "../images/Student.png";

import {
  Tooltip,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from "reactstrap";

export default class SuceesFormStudent extends Component {
  state = {
    unSelectOpacity: 0.4,
    selectOpacity: 1,
    imgvalue: "",
    formData: {
      name: "",
      gender: "",
      dob: "",
      contact: "",
      address: ""
    }
  };

  render() {
    return (
      <>
        {" "}
        <div className="center">
          <Newnav />
          <div className="back3">
            <div style={{ color: "white", textAlign: "center" }}>
              <div className="row">
                <div className="col-lg-4"></div>
                <div className="col-lg-4">
                  <div className="row">
                    {/* <div className="col-lg-6"><img src={Student} alt="" /> </div> */}
                    <div
                      className="col-lg-12"
                      style={{ fontSize: "44px", float: "left" }}
                    >
                      <img style={{ margin: "10px" }} src={Student} alt="" />
                      {/* <img style={{ margin: "10px" }} src={Student} alt="" /> */}
                      Hi,User
                      <br />
                      Congratulation
                    </div>

                    <div className="col-lg-12">
                      <br />
                      <p class="white">
                        {" "}
                        Lorem ipsum, dolor sit amet consectetur adipisicing
                        elit. Expedita omnis reiciendis dolore. Harum,
                        assumenda, earum minima sunt maxime fugiat culpa dolore
                        quas consectetur fugit reprehenderit?
                      </p>
                    </div>
                    <div className="col-lg-12">
                      <br />
                      <Button
                        style={{ background: "#f8fae4", color: "#399B9C" }}
                      >
                        {" "}
                        Download Wallet
                      </Button>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4"></div>
              </div>

              <br />
            </div>
          </div>
        </div>
        <div className="row" style={{ margin: "0px" }}>
          <div className="col-lg-2"></div>
          <div className="col-lg-8">
            <br />
            <p class="subheading" style={{ margin: "50px" }}>
              lorem ipsiummm lorem ipsiummm lorem ipsiummm lorem ipsiummm lorem
              ipsiummm l orem ipsiummm lorem ipsiummm lorem ipsiummm lorem
              ipsiummm lorem ipsiummm lorem ipsiummm lorem ipsiummm lorem
              ipsiummm lorem ipsiummm lorem ipsiummm lorem ipsiummm lorem
              ipsiummm lorem ipsiummm lorem ipsiummm lorem ipsiummm lorem
              ipsiummm lorem ipsiummm lorem ipsiummm
            </p>
          </div>
          <div className="col-lg-2"></div>
        </div>
      </>
    );
  }
}
