import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React, { Component } from 'react'
import Data from '../axiosApi/axiosapi'
import { Button, Modal, Input,Row,Col,ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
class ManufaturesForm extends Component
{

    state={
      modal: false,
      realdata:[],
      sucess:false,
      formdata:{
        vin:"",
        newOwner:"",
       
    
    
      }
    }
    toggle=()=> {
      this.setState(prevState => ({
        modal: !prevState.modal
      }));
    }
    
  
  submit= async(event)=>{
    event.preventDefault();
    await  this.setState({formdata: {...this.state.formdata, vin:this.props.item.Key}})
    
    console.log(this.state.formdata)

    const d= await Data.axiosPost("http://18.222.107.115:3000/owner_transfer",this.state.formdata)
  console.log("asas",d.data.status,d.data.data.status)
 if(d.data.status)
 {
   this.setState({sucess:true})
 }
  window.location.reload();
    
  }
  
  
  render(){
    return(
  <>


<div className="App">
   
    

   <Modal style={{width:"800px",height:"800px",maxWidth:"1000px"}}   isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
<ModalHeader toggle={this.toggle}> Ownership Transfer Form</ModalHeader>
<ModalBody>

<form  onSubmit={this.submit}>

<div class="form-group">
<label for="email">Vin: {this.props.item.key} </label>

<input  value={this.props.item.Key} readonly   onLoad={ async(e)=>{
             await  this.setState({formdata: {...this.state.formdata, vin:e.target.value}})

            } }      required class="form-control" id="vim" placeholder="Enter VIN" name="name" 
            />



</div>

<div class="form-group">
<label for="email">New Owner:</label>
<input  type="name" required class="form-control" id="vim" placeholder="Enter Owner Name" 
onChange={ async(e)=>{  await this.setState({formdata: {...this.state.formdata, newOwner: e.target.value}})

console.log(this.state.formdata)


}} />
</div>


<div style={{textAlign:"center"}}> <button  class="primary_btn">Submit </button></div>

{this.state.sucess && <p  style={{textAlign:"center",color:"lightgreen",fontWeight:"bold"}}> Update is sucessfull</p> }
</form>

</ModalBody>

</Modal>

<Button color="primary" onClick={this.toggle}>{this.props.modaltitle}</Button>



    </div>




</>)
  }

}

export default ManufaturesForm;