import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import Newnav from "../component/newnav";
import college from "../images/College.png";
import student from "../images/Student.png";
import Employer from "../images/Employer.png";
import Lordshire from "../images/lordshire.png";
import { Button, Tooltip, Input } from "reactstrap";
import SuceesFormStudent from "./sucessformstudent";

export default class Signupform extends Component {
  state = {
    unSelectOpacity: 0.4,
    selectOpacity: 1,
    selectvalue: "",
    enter: false
  };

  render() {
    return (
      <div>
        <div className="back2">
          <div style={{ color: "white", textAlign: "center" }}>
            <h1 style={{ color: "white" }}>Choose you category</h1>
            <br />
          </div>
          <div
            className="row"
            style={{ padding: "0px", margin: "0px", textAlign: "center" }}
          >
            <div
              className="col-lg-3"
              style={{
                opacity:
                  this.state.selectvalue == "lordshire"
                    ? this.state.selectOpacity
                    : this.state.unSelectOpacity
              }}
            >
              <img
                style={{ height: "50px" }}
                onClick={() => {
                  this.setState({ selectvalue: "lordshire" });
                  this.setState({ passcol: "lordshire" });
                }}
                src={Lordshire}
                alt=""
              />
              <br />
              <p class="white category"> Lordshire</p>
            </div>
            <div
              className="col-lg-3"
              style={{
                opacity:
                  this.state.selectvalue == "Student"
                    ? this.state.selectOpacity
                    : this.state.unSelectOpacity
              }}
            >
              <img
                onClick={() => {
                  this.setState({ selectvalue: "Student" });
                  this.setState({ passcol: "studentadmin" });
                }}
                src={student}
                alt=""
              />
              <br />
              <p class="white category"> Student</p>
            </div>
            <div
              className="col-lg-3"
              style={{
                opacity:
                  this.state.selectvalue == "collegehome"
                    ? this.state.selectOpacity
                    : this.state.unSelectOpacity
              }}
            >
              <img
                onClick={() => {
                  this.setState({ selectvalue: "collegehome" });
                  this.setState({ passcol: "collegeadmin" });
                }}
                src={college}
                alt=""
              />
              <br />
              <p class="white category"> College</p>
            </div>
            <div
              className="col-lg-3"
              style={{
                opacity:
                  this.state.selectvalue == "Employer"
                    ? this.state.selectOpacity
                    : this.state.unSelectOpacity
              }}
            >
              <img
                onClick={() => {
                  this.setState({ selectvalue: "Employer" });
                  this.setState({ passcol: "employeeadmin" });
                }}
                src={Employer}
                alt=""
              />
              <br />
              <p class="white category">Employer</p>
            </div>
          </div>
        </div>
        <br />
        <div className="row zero" style={{ padding: "0px", marging: "0px" }}>
          <div className="col-lg-4 center"></div>
          <div className="col-lg-4 center">
            <p style={{ color: "black" }}>Please Enter Password</p>
            <br />
            <Input
              type="text"
              placeholder="enter Password"
              onChange={async e => {
                if (
                  this.state.selectvalue == "Employer" &&
                  e.target.value == "employeeadmin"
                ) {
                  await this.setState({ enter: true });
                }
                if (
                  this.state.selectvalue == "collegehome" &&
                  e.target.value == "collegeadmin"
                ) {
                  await this.setState({ enter: true });
                }
                if (
                  this.state.selectvalue == "Student" &&
                  e.target.value == "studentadmin"
                ) {
                  await this.setState({ enter: true });
                }

                if (
                  this.state.selectvalue == "lordshire" &&
                  e.target.value == "lordshire"
                ) {
                  await this.setState({ enter: true });
                }
              }}
            />
          </div>
          <br />
          <div className="col-lg-4 center"></div>
          <div className="col-lg-12 center">
            <Link
              to={{
                pathname: "/signupform",

                state: { show: this.state.selectvalue }
              }}
            >
              <br />
              <Button
                disabled={!this.state.enter ? true : false}
                color="primary"
              >
                {this.state.selectvalue == "" ? "Select Category" : "Continue"}
              </Button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
