import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import Newnav from "../component/newnav";
import Data from "../axiosApi/axiosapi";
import axios from "axios";
import female from "../images/Female.png";
import male from "../images/Male.png";
import qs from "qs";
import Student from "../images/Student.png";
import sucessform from "./sucessformstudent";
import {
  Tooltip,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from "reactstrap";
var wallet = {};
var file;
var data = [];
var data2 = [];
export default class SignupformDeatils extends Component {
  state = {
    unSelectOpacity: 0.4,
    selectOpacity: 1,
    imgvalue: "",
    form: true,
    formData: {
      userName: "",
      gender: "",
      dob: "",
      contact: "",
      address: "",
      orgName: "Lordshire"
    }
  };

  submit = async event => {
    event.preventDefault();
    console.log("clickkk");
    const data1 = {};

    const header = {
      username: this.state.formData.userName,
      gender: this.state.formData.gender,
      dob: this.state.formData.dob,
      contact: this.state.formData.contact,
      address: this.state.formData.address,
      orgname: this.state.formData.orgName,
      wallet_type: this.props.location.state.show
    };
    console.log("clickkk", header);

    await axios
      .post(
        `http://ec2-3-81-149-150.compute-1.amazonaws.com:3000/enroll_user`,
        qs.stringify(header)
      )
      .then(async response => {
        console.log("datatat", response.data);
        wallet = response.data;

        await this.setState({ form: false });
        data.push(JSON.stringify(response.data));

        var properties = { type: "text/json", charset: "UTF-16BE" }; // Specify the file's mime-type.
        try {
          // Specify the filename using the File constructor, but ...
          file = new File(data, "file.json", properties);
        } catch (e) {
          // ... fall back to the Blob constructor if that isn't supported.
          file = new Blob(data, properties);
        }

        var url = URL.createObjectURL(file);
        document.getElementById("link").href = url;
      })
      .catch(error => {
        console.log("datatat", error);
      });
  };

  handleChange = async (event, data) => {
    console.log("name", event, "value", data, this.state.formData);

    console.log("changevaluye", this.state.formData);
  };

  render() {
    console.log(
      "ss",
      this.props.location.state,

      typeof this.props.location.state
    );

    return (
      <>
        {" "}
        <div className="center">
          <Newnav />

          {this.state.form && (
            <div className="back2">
              <div style={{ color: "white", textAlign: "center" }}>
                <h1 style={{ color: "white" }}>
                  {this.props.location.state.show == "collegehome"
                    ? "College Form"
                    : ""}
                  {this.props.location.state.show == "lordshire"
                    ? "Lordshire Form"
                    : ""}
                  {this.props.location.state.show == "Student"
                    ? "Student Form"
                    : ""}
                  {this.props.location.state.show == "Employer"
                    ? "Employer Form"
                    : ""}
                </h1>
                <br />
                <div className="row">
                  <div className="col-lg-4"></div>
                  <div className="col-lg-4">
                    <Form>
                      <FormGroup>
                        <Input
                          onChange={async e => {
                            await this.setState({
                              formData: {
                                ...this.state.formData,
                                userName: e.target.value
                              }
                            });
                          }}
                          type="text"
                          name="Name"
                          id="exampleEmail"
                          placeholder="Name"
                        />
                      </FormGroup>
                      <FormGroup className="center">
                        <img
                          onClick={() => {
                            this.setState({
                              formData: { ...this.state.formData, gender: "M" }
                            });
                          }}
                          style={{
                            opacity:
                              this.state.formData.gender != "M"
                                ? this.state.unSelectOpacity
                                : this.state.selectOpacity
                          }}
                          src={male}
                          alt=""
                        />
                        <img
                          onClick={() => {
                            this.setState({
                              formData: { ...this.state.formData, gender: "F" }
                            });
                          }}
                          style={{
                            opacity:
                              this.state.formData.gender != "F"
                                ? this.state.unSelectOpacity
                                : this.state.selectOpacity
                          }}
                          src={female}
                          alt=""
                        />
                      </FormGroup>

                      <FormGroup>
                        <Input
                          onChange={e => {
                            this.setState({
                              formData: {
                                ...this.state.formData,
                                dob: e.target.value
                              }
                            });
                          }}
                          type="text"
                          name="dob"
                          id="examplePassword"
                          placeholder="Date of Birth"
                        />
                      </FormGroup>

                      <FormGroup>
                        <Input
                          onChange={e => {
                            this.setState({
                              formData: {
                                ...this.state.formData,
                                contact: e.target.value
                              }
                            });
                          }}
                          type="text"
                          name="Contact"
                          id="examplePassword"
                          placeholder="Contact"
                        />
                      </FormGroup>
                      <FormGroup>
                        <Input
                          onChange={e => {
                            this.setState({
                              formData: {
                                ...this.state.formData,
                                address: e.target.value
                              }
                            });
                          }}
                          type="textarea"
                          name="Address"
                          id="examplePassword"
                          placeholder="Address"
                        />
                      </FormGroup>
                    </Form>
                    <Link to={"/sucessformstudent"}>
                      <Button onClick={this.submit} style={{ color: "white" }}>
                        Submit
                      </Button>
                    </Link>
                  </div>

                  <div className="col-lg-4"></div>
                </div>
              </div>
            </div>
          )}
          {!this.state.form && (
            <>
              <div className="center">
                <div className="back3">
                  <div style={{ color: "white", textAlign: "center" }}>
                    <div className="row">
                      <div className="col-lg-4"></div>
                      <div className="col-lg-4">
                        <div className="row">
                          {/* <div className="col-lg-6"><img src={Student} alt="" /> </div> */}
                          <div
                            className="col-lg-12"
                            style={{ fontSize: "44px", float: "left" }}
                          >
                            <img
                              style={{ margin: "10px" }}
                              src={Student}
                              alt=""
                            />
                            {/* <img style={{ margin: "10px" }} src={Student} alt="" /> */}
                            Hi,User
                            <br />
                            Congratulation
                          </div>

                          <div className="col-lg-12">
                            <br />
                            <p class="white">
                              {" "}
                              Issue professional credentials in a digital format
                              that people can hold and share directly with
                              others when needed. These credentials can have
                              branding, meta-data, and verification by QR code
                              to help with in-person situations and global
                              employee movement.
                            </p>
                          </div>
                          <div className="col-lg-12">
                            <br />
                            {/* <Button style={{ background: "#f8fae4", color: "#399B9C" }}

                            onClick={() => {




                              var properties = { type: 'text/json', charset: 'UTF-16BE' }; // Specify the file's mime-type.
                              try {
                                // Specify the filename using the File constructor, but ...
                                file = new File(data, "file.json", properties);
                              } catch (e) {
                                // ... fall back to the Blob constructor if that isn't supported.
                                file = new Blob(data, properties);
                              }
                            }
                            }


                          > Download Wallet</Button> */}
                            <a id="link" target="_blank" download="file.json">
                              <Button
                                style={{
                                  background: "#f8fae4",
                                  color: "#399B9C"
                                }}
                              >
                                {" "}
                                Download Wallet
                              </Button>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-4"></div>
                    </div>

                    <br />
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </>
    );
  }
}
