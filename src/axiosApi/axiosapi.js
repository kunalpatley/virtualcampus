import Check from "../api/genricfunction";
import axios from "axios";
import qs from "qs";
const Data = {
  // getUsers = async () => {
  //     let res = await axios.get("https://reqres.in/api/users?page=1");
  //     let { data } = await res.data;

  // }
  axiosGet: async (endpoint, args) => {
    const config = Check.sethConfigration();
    var data2 = {
      certificate: config.headers.certificate,
      mspid: config.headers.mspId,
      orgname: config.headers.orgName,
      privatekey: config.headers.privateKey,
      type: config.headers.type,
      username: config.headers.userName,
      wallet_type: config.headers.wallet_type
    };
    var newendpoint2 = `http://ec2-3-81-149-150.compute-1.amazonaws.com:3000${endpoint}`;
    console.log("newendpoint", newendpoint2);

    try {
      const resultData = await axios.post(newendpoint2, qs.stringify(data2));
      console.log("resultData", resultData);
      return resultData;
    } catch (err) {
      console.log("err from axiosget", err);
      return err;
    }
  },

  axiosPost: async (endpoint, args) => {
    const config = Check.sethConfigration();
    var newendpoint = `http://ec2-3-81-149-150.compute-1.amazonaws.com:3000${endpoint}`;
    console.log("newendpoint", newendpoint);
    var data = {
      certificate: config.headers.certificate,
      mspid: config.headers.mspId,
      orgname: config.headers.orgName,
      privatekey: config.headers.privateKey,
      type: config.headers.type,
      username: config.headers.userName,
      wallet_type: config.headers.wallet_type
    };
    let finaldata = {
      ...args,
      ...data
    };
    console.log("finaldata", finaldata);
    const resultData = await axios.post(newendpoint, qs.stringify(finaldata));
    return resultData;

    // await axios
    //   .post(endpoint, qs.stringify(finaldata))
    //   .then(async response => {
    //     console.log("datatat", response.data);
    //     return response;
    //   })
    //   .catch(error => {
    //     console.log("datatat", error);
    //     return error;
    //   });

    // axios
    //   .post(endpoint, qs.stringify(finaldata))
    //   .then(res => {
    //     return res;
    //   })
    //   .catch(e => {
    //     return e;
    //   });
  }
};

export default Data;
