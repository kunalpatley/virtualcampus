import React, { Component } from "react";
import Logo from "./images/img-01.png";
import Nav from "./component/nav";
import Login from "./pages/login";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect
} from "react-router-dom";
import Manufatures from "./pages/manufactures";
import Insurance from "./pages/insurance";

import Loginsing from "./pages/loginsign";
import Rto from "./pages/rto";
import Dealer from "./pages/dealer";
import Specific from "./pages/specific";
import K from "./pages/ktest";
import Nhra from "./pages/nhra";
import Hco from "./pages/hco";
import Hcp from "./pages/hcp";
import CollegeHome from "./pages/collegehome";

import HomePage from "./pages/homepage";
import signup from "./form/signupform";
import SignupForm from "./form/signupdeatilform";
import SuceesFormStudent from "./form/sucessformstudent";
import StudentHome from "./pages/studenthome";
import Uploadstudentwallet from "./pages/uploadstudentwallet";
import Collage from "./pages/college";
import Issuecertifcate from "./pages/issuecertifcate";
import AssignExtraCourse from "./pages/assignextraacourse";
import Employee from "./pages/employee";
import componentName from "./pages/h";
import Lordshire from "./pages/lordshire";
var tokenDarta = localStorage.getItem("headerdata");
console.log("tokenDartatokenDartatokenDarta", tokenDarta);
const fakeAuth = {
  isAuthenticated: tokenDarta != null ? true : true,
  //isAuthenticated: tokenDarta!=null ? true : false,
  authenticate(cb) {
    this.isAuthenticated = true;
  },
  signout(cb) {
    this.isAuthenticated = false;
  }
};

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        fakeAuth.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

function Error() {
  return (
    <>
      <div>
        <img src="https://cdn.dribbble.com/users/804852/screenshots/3549548/404.gif" />
      </div>{" "}
    </>
  );
}
class App extends Component {
  render() {
    return (
      <>
        <Router>
          <Switch>
            <Route path="/" exact component={HomePage} />
            <Route path="/nhra" exact component={Nhra} />
            <Route path="/hco" exact component={Hco} />
            <Route path="/hcp" exact component={Hcp} />
            <Route path="/HomePage" exact component={HomePage} />
            <Route path="/signup" exact component={signup} />
            <Route path="/signupform" exact component={SignupForm} />
            <Route path="/college" exact component={Collage} />
            <Route path="/collegehome" exact component={CollegeHome} />
            <Route path="/student" exact component={StudentHome} />
            <Route path="/issuecertificate" exact component={Issuecertifcate} />
            <Route path="/componentName" excat component={componentName} />

            <Route
              path="/assignextracourse"
              exact
              component={AssignExtraCourse}
            />
            <Route path="/Employer" exact component={Employee} />

            <Route
              path="/uploadstudentwallet"
              exact
              component={Uploadstudentwallet}
            />
            <Route
              path="/sucessformstudent"
              exact
              component={SuceesFormStudent}
            />
            <Route path="/lordshire" exact component={Lordshire} />
            <PrivateRoute path="/Manufacturer" exact component={Manufatures} />
            <PrivateRoute path="/insurance" exact component={Insurance} />
            <PrivateRoute path="/rto" exact component={Rto} />
            <PrivateRoute path="/dealer" exact component={Dealer} />
            <PrivateRoute path="/loginsing" exact component={Loginsing} />
            <PrivateRoute
              path="/specificvechical/:vin"
              exact
              component={Specific}
            />

            <PrivateRoute path="/ktest" exact component={K} />
            <PrivateRoute path="*" exact component={Error} />
          </Switch>
        </Router>
      </>
    );
  }
}

export default App;
