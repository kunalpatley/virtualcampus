import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React, { Component } from 'react'
import Nav from '../component/nav'
import RtoMainForm from '../form/rtoMainForm';
import RtoForm from '../form/rtoForm'
import Data from '../axiosApi/axiosapi'
import VechicalHistory from '../component/vechicalhistory';
import BlockchainHash from "../component/blockchainhash";
import RejectPlateNumber from '../form/rejectplatenumber'
export default class Rto extends Component {
  state={
    data:[],
    realdata:[]
  }

  submit=(event)=>{
    alert("sasasas")
    event.preventDefault();
  }

  submitReject= async(arg)=>{
    const args={
      vin:arg
    }
    console.log("sasasas")
   var s= await Data.axiosPost("http://18.222.107.115:3000/reject_plate_number",args)
    window.location.reload();

  }
  async componentDidMount (){
    
    

    const Allvechidata=await Data.axiosGet("http://18.222.107.115:3000/get_all_plate_requests")
   
   
   
   
   var  reformattedArray = Allvechidata.data.data.map((obj) =>({ 
   
         value: JSON.parse(obj.value),
         Key:obj.Key 
       
      }));
    console.log("aaaaaa", reformattedArray)
    this.setState({realdata:reformattedArray})
 
 return reformattedArray;
   
     }
  
  render() {
    return (
      <>
     <Nav/> 
      <section className="about_area section_gap" style={{textAlign:"center"}}>
 
		<div className="container">
			<div className="row justify-content-start align-items-center">
			

				<div className="offset-lg-1 col-lg-10">
					<div className="row ">

          <RtoMainForm modaltitle="Rto Main"  />

					
	
					
					</div>
                  
<br/>
                      
<table style={{textAlign:"center"}}>
  <tr>
  <th>VIN</th>
  <th>Registration Status</th>
    
    
    <th>Vehicle Type</th>
    <th>Model</th>
    
    
    
    <th>Company Name</th>
    <th>Owner Name</th>
    <th>Color</th>
    
    <th>Allocate Number</th>
    <th> Reject Number</th>
    <th>Details</th>
  </tr>
 
{this.state.realdata!=[] && this.state.realdata.map((item) =>{ return( <tr key={item}>
    {console.log("itemitemitemitem",item)}
    <td>{item.value.vin}</td>
    <td>{item.value.requestForPlateNumber}</td>
    <td>{item.value.vehicleType}</td>
    <td>{item.value.modal}</td>
    <td>{item.value.companyName}</td>
    <td>{item.value.owner}</td>
    

    
    
    <td>{item.value.color}</td>
  
     <td>
       
 
       <RtoForm item1={item} modaltitle="Registration Allocation"  /></td>
    
    <td>  <RejectPlateNumber item={item} modaltitle="Reject Registration" /> </td>
    <td><Link 
    to={{pathname:`specificvechical/${item.value.vin}`} }>
     View</Link></td>
</tr>) })
}
</table>

  
				</div>
			</div>
      {/* <BlockchainHash/>  */}
		</div>

	</section>
  </>
    )
  }
}
