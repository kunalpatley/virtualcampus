import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Logo from "../images/img-01.png";
import Logo1 from "../images/icons/brand.png";
import {
  Button,
  Modal,
  Input,
  Row,
  Col,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import React, { Component } from "react";
import Check from "../api/genricfunction";
import Navnav from "../component/newnav";
import LoginPopup from "../form/loginpopup";
import Dealerpolicy from "../form/dealerpolicy";
import Sideimg from "../images/GraphicWEB.png";
import Sideimg2 from "../images/image.png";

export class HomePage extends Component {
  state = {
    finallist: [],
    loading: false,
    errormsg: false
  };

  render() {
    return (
      <>
        <Navnav />

        {/* <div style={{ padding: "80px" }}>  <nav
          style={{ background: "transparent !important" }}

          class="navbar navbar-expand-lg navbar-light bg-inverse">
          <a class="navbar-brand" href="#">Hi,</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#"> <span class="sr-only"></span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"></a>
              </li>


            </ul>
            <form class="form-inline my-2 my-lg-0">



              <LoginPopup modaltitile="Demo" />

            </form>
          </div>
        </nav>


        </div> */}

        <div className="back">
          <div style={{ padding: "80px", color: "white", textAlign: "center" }}>
            <h1 style={{ color: "white" }}>
              The simple most secure way to prove your identity
            </h1>
            <Link to={"/signup"}>
              <button
                style={{
                  background: "white",
                  padding: "10px",
                  borderRadius: "5px"
                }}
              >
                Sign up
              </button>
            </Link>
          </div>
        </div>

        <div style={{ padding: "100px" }}>
          <div
            style={{ margin: "0px", padding: "0px", textAlign: "center" }}
            className="row"
          >
            <div className="col-lg-6">
              <h2 class="mainHeading">
                <b class="bold">Keep</b> your personal deatils{" "}
                <b class="bold"> Private</b> <br />
                and control the <b class="bold">Data</b> you share
              </h2>
              <p class="subheading">
                Lordshire trust our future with candidates every day. Use
                blockchain to find the ones who get it right.
                <br />
                Reputation & Decentralized <br />
                The Decentralized, Reputation ledger allow you to find and
                compare candidates based on how accurate They’ve been in
                academics and past.
              </p>{" "}
            </div>
            <div className="col-lg-6">
              <img src={Sideimg2} />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default HomePage;
