import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Logo from "../images/img-01.png";
import Logo1 from "../images/icons/brand.png";
import bag from "../images/homwbag.png";
import ComponentName from "../pages/h";
import certificate from "../images/certificate.png";
import {
  Button,
  Modal,
  Input,
  Table,
  Row,
  Col,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import Data from "../axiosApi/axiosapi";
import React, { Component } from "react";
import Check from "../api/genricfunction";
import Navnav from "../component/newnav";
import LoginPopup from "../form/loginpopup";
import Dealerpolicy from "../form/dealerpolicy";
import Sideimg from "../images/GraphicWEB.png";

export class collegehome extends Component {
  state = {
    finallist: [],
    loading: false,
    errormsg: false,
    dataNew: []
  };

  async componentDidMount() {
    console.log("sdfsdfsdjhsdfjhsdfjhsf");

    const certificatedata = await Data.axiosPost("/getAllStudents", {});
    console.log("certificatedata", certificatedata);
    if (certificatedata.data)
      this.setState({ dataNew: certificatedata.data.data });
  }

  render() {
    return (
      <>
        <Navnav />

        <div className="back2">
          <div style={{ padding: "80px", color: "white", textAlign: "center" }}>
            <h3 style={{ color: "white" }}>
              Issue professional credentials in a digital format that people can
              hold and share directly with others when needed. These credentials
              can have branding, meta-data, and verification by QR code to help
              with in-person situations and global employee movement.
            </h3>
          </div>
          <div
            style={{ margin: "0px", padding: "0px", textAlign: "center" }}
            className="row"
          >
            <div className="col-lg-3">
              <Link to="/assignextracourse">
                <img src={certificate} />
              </Link>
              <br />
              <p className="white">EXtraa Course</p>
            </div>
            <div className="col-lg-3">
              <Link to="/issuecertificate">
                <img src={certificate} />
              </Link>
              <br />
              <p className="white">Issue certificate</p>
            </div>

            <div className="col-lg-3">
              <Link to="/college">
                {" "}
                <img src={bag} />
              </Link>
              <br />
              <p className="white">ADD STUDENT </p>
            </div>
            <div className="col-lg-3"></div>
          </div>{" "}
        </div>

        <div style={{ padding: "100px" }}>
          <div className="row" style={{ margin: "0px", textAlign: "center" }}>
            <div className="col-lg-1"></div>
            <div className="col-lg-10">
              <br />

              <h3>All Student</h3>

              {this.state.dataNew != [] && (
                <Table>
                  <thead>
                    <tr>
                      <th>STudent ID</th>
                      <th> Studnet Name</th>
                      <th>Address</th>
                      <th>Age</th>
                      <th>Conatct Number</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.dataNew.map(item => {
                      return (
                        <tr>
                          <th>{item.studentId}</th>
                          <td>{item.studentName}</td>
                          <td>{item.sex}</td>
                          <td>{item.age}</td>
                          <td>{item.contactNumber}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              )}
            </div>
            <div className="col-lg-1"></div>
          </div>
        </div>
      </>
    );
  }
}

export default collegehome;
