import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import Newnav from "../component/newnav";
import axios from "axios";
import qs from "qs";
import qrcode from "../images/QR.png";
import BAG from "../images/bag.png";
import bell from "../images/Bell.png";
import { Table } from "reactstrap";
import Document from "../images/Document.png";
import Data from "../axiosApi/axiosapi";
import {
  Tooltip,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from "reactstrap";

var request = require("request");

const bcrypt = require("bcryptjs");
const Cryptr = require("cryptr");
const cryptr = new Cryptr("myTotalySecretKey");
const sha256 = require("sha256");
const QRCode = require("qrcode.react");
var decryptedString = cryptr.decrypt(localStorage.getItem("headerdata"));

var certificate = JSON.parse(decryptedString);
export default class StudentHome extends Component {
  state = {
    dataNew: ""
  };
  async componentDidMount() {
    console.log("sdfsdfsdjhsdfjhsdfjhsf");

    const decryptedString = cryptr.decrypt(localStorage.getItem("headerdata"));

    var certificate1 = JSON.parse(decryptedString);

    const certificatedata = await Data.axiosPost(
      "http://ec2-3-88-117-97.compute-1.amazonaws.com:3000/searchStudent",
      { studentId: sha256(certificate) }
    );
    console.log(
      "certificatedata",
      certificatedata,
      "cert",
      certificatedata.data.certificate
    );
    if (certificatedata.data)
      await this.setState({ dataNew: certificatedata.data.data });
    console.log(this.state.dataNew[0].certificate, "ttttt");
  }

  state = {
    unSelectOpacity: 0.4,
    selectOpacity: 1,
    imgvalue: "",
    formData: {
      name: "",
      gender: "",
      dob: "",
      contact: "",
      address: ""
    },
    dataNew: []
  };

  render() {
    // console.log("ccc", certificate.data.certificate)

    // console.log(sha256(certificate));

    return (
      <>
        {" "}
        <div className="center">
          <Newnav />

          <div className="back3">
            <div style={{ color: "white", textAlign: "center" }}>
              <div className="row">
                <div className="col-lg-4"></div>
                <div className="col-lg-4">
                  <div className="row">
                    {/* <div className="col-lg-6"><img src={Student} alt="" /> </div> */}
                    <div
                      className="col-lg-12"
                      style={{ fontSize: "34px", float: "left" }}
                    >
                      The simple , most
                      <br /> Secure way to prove
                      <br />
                      your identity is{" "}
                      <p style={{ fontSize: "22px", color: "white" }}>
                        {" "}
                        {`${sha256(certificate)}`}
                      </p>
                    </div>

                    <div className="col-lg-12">
                      <br />
                      <p class="white">
                        {" "}
                        Blockchain offers a new public infrastructure for
                        verifying credentials in a manner far more durable,
                        secure, and convenient than relying upon a single
                        authority. Here are just a few reasons to adopt this
                        technology.
                      </p>
                    </div>
                    <div className="col-lg-12">
                      <br />
                    </div>
                  </div>
                </div>
                <div className="col-lg-4"></div>
              </div>

              <br />
            </div>
          </div>
        </div>
        <br />
        <br />
        <div className="row" style={{ margin: "0px", textAlign: "center" }}>
          <div className="col-lg-6">
            <br />
            <div className="row">
              <div className="col-lg-12">
                <h3>My Details</h3>

                {this.state.dataNew != "Empty" &&
                  this.state.dataNew.length > 0 && (
                    <Table>
                      <thead>
                        <tr>
                          <th>STudent ID</th>
                          <th> Studnet Name</th>
                          <th>Address</th>
                          <th>Age</th>
                          <th>Conatct Number</th>
                        </tr>
                      </thead>
                      <tbody>
                        {console.log("sss", this.state.dataNew)}
                        {this.state.dataNew != "Empty" &&
                          this.state.dataNew.length > 0 &&
                          this.state.dataNew.map(item => {
                            return (
                              <tr>
                                <th>{item.studentId}</th>
                                <td>{item.studentName}</td>
                                <td>{item.sex}</td>
                                <td>{item.age}</td>
                                <td>{item.contactNumber}</td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </Table>
                  )}
              </div>
              <div className="col-lg-12">
                <h3>My certificate</h3>

                {this.state.dataNew != "Empty" &&
                  this.state.dataNew.length > 0 && (
                    <Table>
                      <thead>
                        <tr>
                          <th>colleeg Name </th>
                          <th> Degree ID </th>
                          <th>degree Type</th>
                          <th> year of passing</th>
                        </tr>
                      </thead>
                      <tbody>
                        {console.log("sss", this.state.dataNew)}
                        {this.state.dataNew != "Empty" &&
                          this.state.dataNew[0].certificate.length > 0 &&
                          this.state.dataNew[0].certificate.map(item => {
                            return (
                              <tr>
                                <th>{item.collegeName}</th>
                                <td>{item.degreeId}</td>
                                <td>{item.degreeType}</td>
                                <td>{item.yop}</td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </Table>
                  )}
              </div>
            </div>

            <div class="back4">
              <br />
              <p className="white">
                lorem ipsum{" "}
                <img style={{ margin: "0px 10px" }} src={Document} alt="" />
              </p>{" "}
            </div>
            <br />
            <div class="back4">
              <br />{" "}
              <p className="white">
                {" "}
                lorem ipsum
                <img
                  style={{ margin: "0px 10px" }}
                  src={Document}
                  alt=""
                />{" "}
              </p>{" "}
            </div>
          </div>
          <div className="col-lg-1"></div>

          <div
            className="col-lg-3"
            style={{ background: "#f8fae4", padding: " 10px " }}
          >
            <p class="subheading centers" style={{ margin: "0px" }}>
              <br />
              <br />
              <img src={BAG} alt="" />
              <br />
              <br />
              <QRCode value={sha256(certificate)} />,
              <br />
              <br />
              <img src={bell} alt="" />
              <br />
            </p>
          </div>
          <div className="col-lg-2"></div>
        </div>
        <div></div>
      </>
    );
  }
}
