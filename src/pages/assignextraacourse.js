import React, { Component, useState } from "react";
import classnames from "classnames";
import { Link } from "react-router-dom";
import Data from "../axiosApi/axiosapi";
import qs from "qs";
import axios from "axios";
import {
  Form,
  Container,
  TabContent,
  Table,
  FormGroup,
  Label,
  Input,
  Jumbotron,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col
} from "reactstrap";

import Check from "../api/genricfunction";
export default class AssignExtraaCourse extends Component {
  state = {
    hcoKey: "",
    data3: "",
    dataArray: [],
    formData: {
      studentId: "",

      orgname: "Lordshire",
      courseId: ""
    }
  };

  async componentDidMount() {
    alert("dd");
    var d = await Data.axiosGet(`/showExtraCourses`);
    console.log("result is", d);
    console.log("datasasasass ", d);
    console.log("resultDataresultDataresultDataresultDataresultData", d.data);
    if (d.data) {
      await this.setState({ dataArray: d.data.data });
    }
  }
  submit = async () => {
    const config = Check.sethConfigration();

    var data2 = {
      certificate: config.headers.certificate,
      mspid: config.headers.mspId,
      orgname: config.headers.orgName,
      privatekey: config.headers.privateKey,
      type: config.headers.type,
      username: config.headers.userName,
      wallet_type: config.headers.wallet_type
    };

    var finaldata = {
      ...data2,
      ...this.state.formData
    };
    console.log("api hit", finaldata);
    var d2 = await Data.axiosPost(`/assignExtraCourses`, this.state.formData);
    console.log("jjjjjd", d2.data.status);

    if (d2.data) {
      await this.setState({ data3: d2.data.data.status });
    }
    console.log("wwww", this.state.data3);
    // axios
    //   .post(
    //     `http://ec2-13-127-177-127.ap-south-1.compute.amazonaws.com:3000/enrollStudentToCollege`,
    //     qs.stringify(finaldata)
    //   )
    //   .then(async response => {
    //     console.log("datatat", response.data);
    //   })
    //   .catch(error => {
    //     console.log("datatat", error);
    //   });
  };

  render() {
    return (
      <>
        <div className="back2">
          <div style={{ color: "white", textAlign: "center" }}>
            <h1 style={{ color: "white" }}>Student Registration Form</h1>
            <br />
            <div className="row">
              <div className="col-lg-4"></div>
              <div className="col-lg-4">
                <Form>
                  <FormGroup>
                    <Input
                      onChange={async e => {
                        await this.setState({
                          formData: {
                            ...this.state.formData,
                            studentId: e.target.value
                          }
                        });
                      }}
                      type="text"
                      name="Name"
                      id="exampleEmail"
                      placeholder="Student id"
                    />
                  </FormGroup>

                  {/* <FormGroup>
                    <Input
                      onChange={async e => {
                        await this.setState({
                          formData: {
                            ...this.state.formData,
                            studentName: e.target.value
                          }
                        });
                      }}
                      type="text"
                      name="Name"
                      id="exampleEmail"
                      placeholder="Student Name"
                    />
                  </FormGroup> */}

                  <FormGroup>
                    <Input
                      type="select"
                      onChange={async e => {
                        await this.setState({
                          formData: {
                            ...this.state.formData,
                            courseId: `Course${e.target.value}`
                          }
                        });
                      }}
                      name="Address"
                      id="examplePassword"
                      placeholder=" student gender"
                    >
                      {console.log(this.state.dataArray)}
                      {this.state.dataArray.length > 0 &&
                        this.state.dataArray.map(item => {
                          console.log("sss", item);
                          return (
                            <option value={item.courseId}>
                              {item.courseName}
                            </option>
                          );
                        })}
                    </Input>
                  </FormGroup>
                </Form>

                <Button onClick={this.submit} style={{ color: "white" }}>
                  Submit
                </Button>
                {this.state.data3 != "" && (
                  <p className="white" style={{ fontSize: "22px" }}>
                    Registration sucessfully Done
                  </p>
                )}
              </div>

              <div className="col-lg-4"></div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
