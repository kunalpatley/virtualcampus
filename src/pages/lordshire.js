import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Logo from "../images/img-01.png";
import Logo1 from "../images/icons/brand.png";
import bag from "../images/homwbag.png";
import ComponentName from "../pages/h";
import certificate from "../images/certificate.png";
import {
  Button,
  Modal,
  Input,
  Table,
  Row,
  Col,
  ModalHeader,
  ModalBody,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardLink,
  CardTitle,
  CardSubtitle,
  ModalFooter
} from "reactstrap";
import Data from "../axiosApi/axiosapi";
import React, { Component } from "react";
import Check from "../api/genricfunction";
import Navnav from "../component/newnav";
import LoginPopup from "../form/loginpopup";
import Dealerpolicy from "../form/dealerpolicy";
import Sideimg from "../images/GraphicWEB.png";

export default class Lordshire extends Component {
  state = {
    modal: false,
    finallist: [],
    loading: false,
    errormsg: false,
    dataNew: [],
    dataSet: []
  };

  async componentDidMount() {
    console.log("sdfsdfsdjhsdfjhsdfjhsf");

    const certificatedata = await Data.axiosPost("/getAllStudents", {});
    console.log("certificatedata", certificatedata);
    if (certificatedata.data)
      this.setState({ dataNew: certificatedata.data.data });
  }

  render() {
    return (
      <>
        <Navnav />

        <div className="back2">
          <div style={{ padding: "80px", color: "white", textAlign: "center" }}>
            <h3 style={{ color: "white" }}>
              Lordshire helps you get the data you need from your users, our
              users or anywhere else on the market. Making use of platform
              &Extensive Network of partner. We can deliver any data from those
              users you need.
            </h3>
          </div>
        </div>

        <div style={{ padding: "100px" }}>
          <div className="row" style={{ margin: "0px", textAlign: "center" }}>
            <div className="col-lg-1"></div>
            <div className="col-lg-10">
              <br />

              <h3>All Student</h3>

              {this.state.dataNew != [] && (
                <Table>
                  <thead>
                    <tr>
                      <th>STudent ID</th>
                      <th> Studnet Name</th>
                      <th>Address</th>
                      <th>Age</th>
                      <th>Conatct Number</th>
                      <th>View</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.dataNew.length > 0 &&
                      this.state.dataNew.map(item => {
                        return (
                          <tr>
                            <th>{item.studentId}</th>
                            <td>{item.studentName}</td>
                            <td>{item.sex}</td>
                            <td>{item.age}</td>
                            <td>{item.contactNumber}</td>
                            <td>
                              <ModalLordhisre item={item} />
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              )}
            </div>

            <div className="col-lg-1"></div>
          </div>
        </div>
      </>
    );
  }
}

class ModalLordhisre extends Component {
  async componentDidMount() {
    const certificatedata1 = await Data.axiosPost("/getAllStudents", {
      studentId: this.props.item.studentId
    });
    console.log("mmmm", certificatedata1.data.data);
    this.setState({ dataSet: certificatedata1.data.data });
  }
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      finallist: [],
      loading: false,
      errormsg: false,
      dataNew: [],
      dataSet: [],
      Degreetrue: false
    };

    this.toggle = this.toggle.bind(this);
  }
  setLordshireApproved = async data => {
    alert(this.props.item.studentId);
    const Degreetrue = await Data.axiosPost("/setLordshireApproved", {
      studentId: this.props.item.studentId
    });
    console.log("Degreetrue", Degreetrue.data.status);
    this.setState({ Degreetrue: Degreetrue.data.status });
  };

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  render() {
    return (
      <>
        <Button color="danger" onClick={this.toggle}>
          View
        </Button>
        <Modal
          style={{ maxWidth: "1000px" }}
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
          <ModalBody>
            <Table>
              <thead>
                <tr>
                  <th>STudent ID</th>
                  <th> Studnet Name</th>
                  <th>Address</th>
                  <th>Age</th>
                  <th>Conatct Number</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataSet.length > 0 &&
                  this.state.dataSet.map(item1 => {
                    console.log("item1", item1);
                    return (
                      item1.studentId == this.props.item.studentId && (
                        <>
                          <tr>
                            <th style={{ fontSize: "10px" }}>
                              {" "}
                              {item1.studentId}
                            </th>
                            <td>{item1.studentName}</td>
                            <td>{item1.sex}</td>
                            <td>{item1.age}</td>
                            <td>{item1.contactNumber}</td>
                          </tr>
                        </>
                      )
                    );
                  })}
              </tbody>
            </Table>
            {this.state.dataSet.length > 0 &&
              this.state.dataSet.map(item1 => {
                console.log("item1", item1);
                return (
                  item1.studentId == this.props.item.studentId && (
                    <>
                      <Card>
                        <CardBody>
                          <CardTitle>
                            <h4>Certificate</h4>
                          </CardTitle>
                        </CardBody>

                        <CardBody>
                          <CardText>
                            {item1.certificate.length > 0 ? (
                              item1.certificate.map(itemcertificate => {
                                return (
                                  <>
                                    <div className="row">
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          collegeName:
                                        </b>
                                        {itemcertificate.collegeName}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          degreeId:
                                        </b>
                                        {itemcertificate.degreeId}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          degreeType:
                                        </b>
                                        {itemcertificate.degreeType}
                                      </div>
                                    </div>
                                  </>
                                );
                              })
                            ) : (
                              <h2>NO Certificate Issued</h2>
                            )}
                          </CardText>
                        </CardBody>
                      </Card>

                      <Card>
                        <CardBody>
                          <CardTitle>
                            <h4> Extraa Course</h4>
                          </CardTitle>
                        </CardBody>

                        <CardBody>
                          <CardText>
                            {item1.extraCourses.length > 0 ? (
                              item1.extraCourses.map(itemextracourse => {
                                return (
                                  <>
                                    <div className="row">
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          courseId:
                                        </b>
                                        {itemextracourse.courseId}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          courseName:
                                        </b>

                                        {itemextracourse.courseName}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          docType:
                                        </b>
                                        {itemextracourse.docType}
                                      </div>
                                    </div>
                                  </>
                                );
                              })
                            ) : (
                              <h2>No Course Issued</h2>
                            )}
                          </CardText>
                        </CardBody>
                      </Card>
                    </>
                  )
                );
              })}
            <br />

            <Button onClick={this.setLordshireApproved} color="success">
              Assign Degree True
            </Button>
            <br />
            <p
              style={{
                fontSize: "18px",
                color: "green",
                fontweight: "900",
                textAlign: "center"
              }}
            >
              {" "}
              {this.state.Degreetrue && " Lordhsire Approved"}
            </p>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
