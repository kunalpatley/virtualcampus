import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React, { Component } from 'react'

import Nav from '../component/nav'
import InsuranceMainForm from '../form/insuranceMainForm'
import InsuranceForm from '../form/insuranceForm'
import Data from '../axiosApi/axiosapi'
import VechicalHistory from '../component/vechicalhistory';
import BlockchainHash from "../component/blockchainhash";
import Rejectinsurance from '../form/rejectinsurance'
export default class Insurance extends Component {
  state={
    data:[],
    realdata:[]
  }
  submitReject=(arg)=>{
    const args={
      vin:arg
    }
    console.log("sasasas")
    Data.axiosPost("http://18.222.107.115:3000/reject_policy_number",args)

  }

  submit=(event)=>{
    alert("sasasas")
    event.preventDefault();
  }
  async componentDidMount (){
    
    

    const Allvechidata=await Data.axiosGet("http://18.222.107.115:3000/get_all_policy_requests")
   
   
   
   
   var  reformattedArray = Allvechidata.data.data.map((obj) =>({ 
   
         value: JSON.parse(obj.value),
         Key:obj.Key 
       
      }));
    console.log("aaaaaa", reformattedArray)
    this.setState({realdata:reformattedArray})
 
 return reformattedArray;
   
     }

  render() {
    return (
      <>
     <Nav/>
      <section className="about_area section_gap" style={{textAlign:"center"}}>
 
		<div className="container">
			<div className="row justify-content-start align-items-center">
			

				<div className="offset-lg-1 col-lg-10">
					<div className="row ">

                     <InsuranceMainForm modaltitle="Insurance MainForm" />    
				
					
					</div>
                  
<br/>
                       
<table style={{textAlign:"center"}}>
  <tr>
  <th>VIN</th>
  <th> Policy Request</th>
    <th>Company</th>
    <th>Owner</th>
    <th>Vehicle Type</th>
    <th>Model</th>
    <th>Color</th>
    <th>Policy Number</th>
        <th>Reject</th>
    <th>Details</th>
    
   

    
  </tr>
 
{this.state.realdata.map((item) =>{ return( <tr key={item}>
    {console.log("itemitemitemitem",item)}
    <td>{item.value.vin}</td>
    <td>{item.value.requestForPolicyNumber}</td>
    <td>{item.value.companyName}</td>
    <td>{item.value.owner}</td>
    <td>{item.value.vehicleType}</td>

    <td>{item.value.modal}</td>
    
    <td>{item.value.color}</td>
   
     <td><InsuranceForm item={item} modaltitle="Assign Policy Number"/></td>
     <td>  {/* <td><button  class="primary_btn" type="button" onClick={()=>{
      this.submitReject(item.value.vin)
     }}>reject</button> */}
     
     
     <Rejectinsurance item={item} modaltitle="Reject Policy" />
     </td>
      <td><Link 
    to={{pathname:`specificvechical/${item.value.vin}`} }>
     View</Link></td>
     
</tr>) })
}
</table>

				</div>
			</div>
      {/* <BlockchainHash/>  */}
		</div>






	</section>
  </>
    )
  }
}
