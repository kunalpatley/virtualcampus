import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Logo from "../images/img-01.png";
import Logo1 from "../images/icons/brand.png";
import bag from "../images/homwbag.png";
import certificate from "../images/certificate.png";
import {
  Button,
  Modal,
  Input,
  Table,
  Row,
  Col,
  ModalHeader,
  ModalBody,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardLink,
  CardTitle,
  CardSubtitle,
  ModalFooter
} from "reactstrap";
import Data from "../axiosApi/axiosapi";
import React, { Component } from "react";
import Check from "../api/genricfunction";
import Navnav from "../component/newnav";
import LoginPopup from "../form/loginpopup";
import Dealerpolicy from "../form/dealerpolicy";
import Sideimg from "../images/GraphicWEB.png";

export class Employee extends Component {
  state = {
    finallist: [],
    loading: false,
    errormsg: false,
    dataNew: [],
    Coursedata: [],
    degreeData: []
  };

  async componentDidMount() {
    console.log("sdfsdfsdjhsdfjhsdfjhsf");

    const certificatedata = await Data.axiosPost(
      "/getAllStudentsByCourseType",
      {}
    );
    console.log("certificatedata", certificatedata);
    if (certificatedata.data)
      this.setState({ dataNew: certificatedata.data.data });
  }
  degreefilter = async data => {
    console.log("data");
    const degreesdata = await Data.axiosPost("/getAllStudentsByDegreeType", {
      degree: data
    });
    console.log("degreesdata", degreesdata);
    if (degreesdata.data) {
      this.setState({ degreeData: degreesdata.data.data });
    }
    console.log("degreesdata.data.data", degreesdata.data.data);
  };
  coursefilter = async data => {
    console.log("data");
    const degreesdata = await Data.axiosPost("/getAllStudentsByCourseType", {
      courseId: data
    });
    console.log("degreesdata", degreesdata);
    if (degreesdata.data) {
      this.setState({
        Coursedata:
          degreesdata.data.data == "Empty" ? [] : degreesdata.data.data
      });
    }
    console.log("Coursedata.Coursedata.Coursedata", degreesdata.data.data);
  };
  render() {
    return (
      <>
        <Navnav />

        <div className="back2">
          <div style={{ padding: "80px", color: "white", textAlign: "center" }}>
            <h3 style={{ color: "white" }}>
              Provide educational and career history verified by blockchain
              technology in a transparent and traceable way. Ease of Sharing –
              With the help of blockchain technology, students can share their
              degrees and related certificates with the college(s) with ease.
              The rights to allow a college/university with the access to the
              information stay with the student (applicant).
            </h3>
          </div>
        </div>

        <div style={{ padding: "100px" }}>
          <div className="row" style={{ margin: "0px", textAlign: "center" }}>
            <div className="col-lg-12">
              <br />

              <h3> Students By Course</h3>
              <div className="row">
                <br />

                <div className="col-lg-12">
                  <Input
                    onChange={e => {
                      this.setState({ Degreefilter: e.target.value });

                      this.coursefilter(e.target.value);
                    }}
                    type="select"
                    placeholder="enter Degree "
                  >
                    <option value="Btech">Select Course</option>
                    <option value="Course0">Programming</option>
                    <option value="Course1">Sports</option>
                    <option value="Course2">Art & craft</option>
                    <option value="Course3">Photography</option>
                  </Input>
                </div>
              </div>
              <div></div>
              <br />

              {this.state.Coursedata.length > 0 ? (
                <Table>
                  <thead>
                    <tr>
                      <th>Student ID</th>
                      <th> Student Name</th>
                      <th>Address</th>
                      <th>Age</th>
                      <th>Conatct Number</th>
                      <th>View</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.Coursedata.length > 0 &&
                      this.state.Coursedata.map(item => {
                        return (
                          <tr>
                            <th style={{ fontSize: "16px" }}>
                              {item.studentId}
                            </th>
                            <td>{item.studentName}</td>
                            <td>{item.sex}</td>
                            <td>{item.age}</td>
                            <td>{item.contactNumber}</td>
                            <td>
                              <td>
                                <ModalLordhisre item={item} />
                              </td>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              ) : (
                <h2>No Data Found</h2>
              )}
            </div>
            <div className="col-lg-12">
              <br />

              <h3> Students By Degree </h3>
              <div className="row">
                <br />

                <div className="col-lg-12">
                  <Input
                    onChange={e => {
                      this.setState({ Degreefilter: e.target.value });

                      this.degreefilter(e.target.value);
                    }}
                    type="select"
                    placeholder="enter Degree "
                  >
                    <option value="">Select Degree</option>
                    <option value="Btech">Btech</option>
                    <option value="Mba">MBA</option>
                  </Input>
                </div>
              </div>
              <div></div>
              <br />

              {this.state.degreeData.length > 0 ? (
                <Table>
                  <thead>
                    <tr>
                      <th style={{ fontSize: "16px" }}>Student ID</th>
                      <th> Studnet Name</th>
                      <th>Address</th>
                      <th>Age</th>
                      <th>Conatct Number</th>
                      <th>View</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.degreeData.length > 0 &&
                      this.state.degreeData.map(item => {
                        return (
                          <tr>
                            <th>{item.studentId}</th>
                            <td>{item.studentName}</td>
                            <td>{item.sex}</td>
                            <td>{item.age}</td>
                            <td>{item.contactNumber}</td>
                            <td>
                              <td>
                                <ModalLordhisre item={item} />
                              </td>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              ) : (
                <h2>No Data Found</h2>
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Employee;

class ModalLordhisre extends Component {
  async componentDidMount() {
    const certificatedata1 = await Data.axiosPost("/getAllStudents", {
      studentId: this.props.item.studentId
    });
    console.log("mmmm", certificatedata1.data.data);
    this.setState({ dataSet: certificatedata1.data.data });
  }
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      finallist: [],
      loading: false,
      errormsg: false,
      dataNew: [],
      dataSet: [],
      Degreetrue: false
    };

    this.toggle = this.toggle.bind(this);
  }
  setLordshireApproved = async data => {
    alert(this.props.item.studentId);
    const Degreetrue = await Data.axiosPost("/setLordshireApproved", {
      studentId: this.props.item.studentId
    });
    console.log("Degreetrue", Degreetrue.data.status);
    this.setState({ Degreetrue: Degreetrue.data.status });
  };

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  render() {
    return (
      <>
        <Button color="danger" onClick={this.toggle}>
          View
        </Button>
        <Modal
          style={{ maxWidth: "1000px" }}
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
          <ModalBody>
            <Table>
              <thead>
                <tr>
                  <th>STudent ID</th>
                  <th> Studnet Name</th>
                  <th>Address</th>
                  <th>Age</th>
                  <th>Conatct Number</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataSet.length > 0 &&
                  this.state.dataSet.map(item1 => {
                    console.log("item1", item1);
                    return (
                      item1.studentId == this.props.item.studentId && (
                        <>
                          <tr>
                            <th style={{ fontSize: "10px" }}>
                              {" "}
                              {item1.studentId}
                            </th>
                            <td>{item1.studentName}</td>
                            <td>{item1.sex}</td>
                            <td>{item1.age}</td>
                            <td>{item1.contactNumber}</td>
                          </tr>
                        </>
                      )
                    );
                  })}
              </tbody>
            </Table>
            {this.state.dataSet.length > 0 &&
              this.state.dataSet.map(item1 => {
                console.log("item1", item1);
                return (
                  item1.studentId == this.props.item.studentId && (
                    <>
                      <Card>
                        <CardBody>
                          <CardTitle>
                            <h4>Certificate</h4>
                          </CardTitle>
                        </CardBody>

                        <CardBody>
                          <CardText>
                            {item1.certificate.length > 0 ? (
                              item1.certificate.map(itemcertificate => {
                                return (
                                  <>
                                    <div className="row">
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          collegeName:
                                        </b>
                                        {itemcertificate.collegeName}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          degreeId:
                                        </b>
                                        {itemcertificate.degreeId}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          degreeType:
                                        </b>
                                        {itemcertificate.degreeType}
                                      </div>
                                    </div>
                                  </>
                                );
                              })
                            ) : (
                              <h2>NO Certificate Issued</h2>
                            )}
                          </CardText>
                        </CardBody>
                      </Card>

                      <Card>
                        <CardBody>
                          <CardTitle>
                            <h4> Extraa Course</h4>
                          </CardTitle>
                        </CardBody>

                        <CardBody>
                          <CardText>
                            {item1.extraCourses.length > 0 ? (
                              item1.extraCourses.map(itemextracourse => {
                                return (
                                  <>
                                    <div className="row">
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          courseId:
                                        </b>
                                        {itemextracourse.courseId}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          courseName:
                                        </b>

                                        {itemextracourse.courseName}
                                      </div>
                                      <div className="col-lg-4">
                                        <b
                                          style={{
                                            fontweight: 900,
                                            color: "black"
                                          }}
                                        >
                                          docType:
                                        </b>
                                        {itemextracourse.docType}
                                      </div>
                                    </div>
                                  </>
                                );
                              })
                            ) : (
                              <h2>No Course Issued</h2>
                            )}
                          </CardText>
                        </CardBody>
                      </Card>
                    </>
                  )
                );
              })}
            {this.props.item.LordshireApproved ? (
              <p
                style={{
                  color: "green",
                  textAlign: "center",
                  fontSize: "22px"
                }}
              >
                Degree Approved By Lordhsire{" "}
              </p>
            ) : (
              <p
                style={{ color: "red", textAlign: "center", fontSize: "22px" }}
              >
                Degree Not Approved Yet By Lordhsire
              </p>
            )}
            <br />
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
