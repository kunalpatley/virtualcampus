import React from 'react';
import classnames from 'classnames';
import { Container,TabContent,Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col  } from 'reactstrap';

export default class NHRA extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (
      <Container style={{padding:"50px"}}>




        <Row>
          <Col xs="1" sm="1" md="1" ></Col>
          <Col xs="10" sm="10" md="10"> 
          <div>
          <Row style={{textAlign:"center"}}>
            <h1>Tittle list</h1>   
       
            </Row>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Tab1
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              TBA@
            </NavLink>
          </NavItem>
        
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
            tab3
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
         
          <TabPane tabId="1">
            
            <Row>
              <br/>
              <Col sm="12" style={{textAlign:"center"}}>
                <br/>
                <br/>
                <div style={{margin:"15px"}}>   



                <h1>Tittle list</h1>         
                            <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </Table>
      </div>

 


              </Col>
             
            </Row>
          </TabPane>


          <TabPane tabId="2">
            
            <Row>
              <br/>
              <Col sm="12" style={{textAlign:"center"}}>
                <br/>
                <br/>
                <div style={{margin:"15px"}}>   



                <h1>Tittle list2</h1>         
                            <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </Table>
      </div>

 


              </Col>
             
            </Row>
          </TabPane>
          <TabPane tabId="3">
            
            <Row>
              <br/>
              <Col sm="12" style={{textAlign:"center"}}>
                <br/>
                <br/>
                <div style={{margin:"15px"}}>   



                <h1>Tittle list3</h1>         
                            <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </Table>
      </div>

 


              </Col>
             
            </Row>
          </TabPane>
         
        </TabContent>
      </div></Col>
          <Col sm="1"  md="1"></Col>
        </Row>
        
        
      </Container>
    );
  }
}