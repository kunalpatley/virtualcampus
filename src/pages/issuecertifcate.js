import React, { Component, useState } from "react";
import classnames from "classnames";
import { Link } from "react-router-dom";
import Data from "../axiosApi/axiosapi";
import qs from "qs";

import axios from "axios";

import {
  Form,
  Container,
  TabContent,
  Table,
  FormGroup,
  Label,
  Input,
  Jumbotron,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col
} from "reactstrap";

import Check from "../api/genricfunction";

var marksBtech = [];
var marksMba = [];
var d;
export default class Issuecertifcate extends Component {
  state = {
    hcoKey: "",
    currentsem: "sem1",
    degreeType: "Btech",
    bsemMark1: "",
    bsemMark2: "",
    bsemMark3: "",
    bsemMark4: "",
    MsemMark1: "",
    MsemMark2: "",
    byear1: "",
    byear2: "",
    byear3: "",
    byear4: "",
    Myear1: "",
    Myear2: "",

    sem1mark: "",
    sem2mark: "",
    sem3mark: "",
    btechyear1: "",
    data3: "",
    formData: {
      studentId: "",
      degreeType: "",
      yop: "",

      orgname: "Lordshire"
    },
    semesterResult: []
  };

  submit = async () => {
    marksBtech = await {
      semesterResult: [
        { sem1: "sem1", year: this.state.byear1, marks: this.state.bsemMark1 },
        { sem2: "sem2", year: this.state.byear2, marks: this.state.bsemMark2 },
        { sem3: "sem3", year: this.state.byear3, marks: this.state.bsemMark3 },
        { sem4: "sem4", year: this.state.byear4, marks: this.state.bsemMark4 }
      ]
    };
    marksMba = await {
      semesterResult: [
        { sem1: "sem1", year: this.state.Myear1, marks: this.state.MsemMark1 },
        { sem2: "sem2", year: this.state.Myear2, marks: this.state.MsemMark2 }
      ]
    };

    var finaldataMba = {
      ...this.state.formData,
      ...marksMba,
      ...{ degreeType: "Btech" }
    };
    var finaldataBtach = {
      ...this.state.formData,
      ...marksBtech,
      ...{ degreeType: "Mba" }
    };

    if (this.state.degreeType == "Mba") {
      alert("mab");
      console.log("finaldataBtach", finaldataMba);
      d = await Data.axiosPost(`/issueDegree`, finaldataMba);
    }
    if (this.state.degreeType == "Btech") {
      alert("btech");
      console.log("finaldataBtach", finaldataBtach);
      d = await Data.axiosPost(`/issueDegree`, finaldataBtach);
    }

    console.log("api hit", this.state.formData);

    console.log("data is ", d);
    if (d.data) {
      this.setState({ data3: d.data });
    }
    console.log("wwww", this.state.data3);
    // axios
    //   .post(
    //     `http://ec2-13-127-177-127.ap-south-1.compute.amazonaws.com:3000/enrollStudentToCollege`,
    //     qs.stringify(finaldata)
    //   )
    //   .then(async response => {
    //     console.log("datatat", response.data);
    //   })
    //   .catch(error => {
    //     console.log("datatat", error);
    //   });
  };

  render() {
    return (
      <>
        <div className="back2">
          <div style={{ color: "white", textAlign: "center" }}>
            <h1 style={{ color: "white" }}> Certificate Issue Form</h1>
            <br />
            <div className="row">
              <div className="col-lg-4"></div>
              <div className="col-lg-4">
                <Form>
                  <FormGroup>
                    <Input
                      onChange={async e => {
                        await this.setState({
                          formData: {
                            ...this.state.formData,
                            studentId: e.target.value
                          }
                        });
                      }}
                      type="text"
                      name="Name"
                      id="exampleEmail"
                      placeholder="Student id"
                    />
                  </FormGroup>

                  <FormGroup>
                    <Input
                      onChange={async e => {
                        await this.setState({
                          degreeType: e.target.value
                        });
                      }}
                      type="select"
                      name="select"
                      id="exampleSelect"
                    >
                      <option value="Btech">Btech</option>

                      <option value="Mba">Mba</option>
                    </Input>
                  </FormGroup>
                  {this.state.degreeType == "Btech" && (
                    <>
                      <FormGroup>
                        <div className="row">
                          <div className="col-lg-4">
                            <Input
                              value="sem1"
                              type="text"
                              name="select"
                              id="exampleSelect"
                            ></Input>
                          </div>
                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  byear1: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Year"
                            />
                          </div>

                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  bsemMark1: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Marks"
                            />
                          </div>
                        </div>
                      </FormGroup>
                      <FormGroup>
                        <div className="row">
                          <div className="col-lg-4">
                            <Input
                              value="sem2"
                              type="text"
                              name="select"
                              id="exampleSelect"
                            ></Input>
                          </div>
                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  byear2: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Year"
                            />
                          </div>

                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                console.log(this.state.formData.dob);

                                await this.setState({
                                  bsemMark2: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Marks"
                            />
                          </div>
                        </div>
                      </FormGroup>
                      <FormGroup>
                        <div className="row">
                          <div className="col-lg-4">
                            <Input
                              value="sem3"
                              type="text"
                              name="select"
                              id="exampleSelect"
                            ></Input>
                          </div>
                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  byear3: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Year"
                            />
                          </div>

                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                console.log(this.state.formData.dob);

                                await this.setState({
                                  bsemMark3: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Marks"
                            />
                          </div>
                        </div>
                      </FormGroup>
                      <FormGroup>
                        <div className="row">
                          <div className="col-lg-4">
                            <Input
                              value="sem4"
                              type="text"
                              name="select"
                              id="exampleSelect"
                            ></Input>
                          </div>
                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  byear4: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Year"
                            />
                          </div>

                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                console.log(this.state.formData.dob);

                                await this.setState({
                                  bsemMark4: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Marks"
                            />
                          </div>
                        </div>
                      </FormGroup>
                    </>
                  )}
                  {this.state.degreeType == "Mba" && (
                    <>
                      <FormGroup>
                        <div className="row">
                          <div className="col-lg-4">
                            <Input
                              value="sem1"
                              type="text"
                              name="select"
                              id="exampleSelect"
                            ></Input>
                          </div>
                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  Myear1: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Year"
                            />
                          </div>

                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                console.log(this.state.formData.dob);

                                await this.setState({
                                  MsemMark1: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Marks"
                            />
                          </div>
                        </div>
                      </FormGroup>
                      <FormGroup>
                        <div className="row">
                          <div className="col-lg-4">
                            <Input
                              value="sem1"
                              type="text"
                              name="select"
                              id="exampleSelect"
                            ></Input>
                          </div>
                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                await this.setState({
                                  Myear2: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Year"
                            />
                          </div>

                          <div className="col-lg-4">
                            {" "}
                            <Input
                              onChange={async e => {
                                console.log(this.state.formData.dob);

                                await this.setState({
                                  MsemMark2: e.target.value
                                });
                              }}
                              type="text"
                              name="dob"
                              id="examplePassword"
                              placeholder="Marks"
                            />
                          </div>
                        </div>
                      </FormGroup>
                    </>
                  )}

                  <FormGroup>
                    <Input
                      onChange={e => {
                        console.log(this.state.formData.dob);

                        this.setState({
                          formData: {
                            ...this.state.formData,
                            yop: e.target.value
                          }
                        });
                      }}
                      type="text"
                      name="dob"
                      id="examplePassword"
                      placeholder="Year of passing"
                    />
                  </FormGroup>
                </Form>

                <Button onClick={this.submit} style={{ color: "white" }}>
                  Submit
                </Button>
                {this.state.data3 != "" && (
                  <p className="white" style={{ fontSize: "22px" }}>
                    Certificate sucessfully Issued
                  </p>
                )}
              </div>

              <div className="col-lg-4"></div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
