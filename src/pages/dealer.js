import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React, { Component } from 'react'
import Nav from '../component/nav'
import DealerForm from '../form/dealerForm'
import DealerMainForm from '../form/dealerMainForm'
import BlockchainHash from "../component/blockchainhash";
import axios from 'axios';
import  qs from "qs";
import Dealerpolicy from '../form/dealerpolicy'
import Data from '../axiosApi/axiosapi'


const bcrypt =require('bcryptjs')
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');

export default class Dealer extends Component {
  state={
    data:[],
    realdata:[]
  }
  

  submit=(event)=>{
    alert("sasasas")
    event.preventDefault();
  }
 
  
  async componentDidMount (){
    
    

   const Allvechidata=await Data.axiosGet("http://18.222.107.115:3000/query_all_vehicles")
  
  
  
  
  var  reformattedArray = Allvechidata.data.data.map((obj) =>({ 
  
        value: JSON.parse(obj.value),
        Key:obj.Key 
      
     }));
   console.log("aaaaaa", reformattedArray)
   this.setState({realdata:reformattedArray})

return reformattedArray;
  
    }
  render() {
    return (
      <>
      <Nav/>
      <section className="about_area section_gap" style={{textAlign:"center"}}>
 
		<div className="container">
			<div className="row justify-content-start align-items-center">
      

				<div className="offset-lg-1 col-lg-10">
					<div className="row ">

                 <DealerMainForm modaltitle="Main Dealer Form" />       
										</div>
                  
<br/>
                           
                       
<table style={{textAlign:"center"}}>
  <tr>
  <th>Vin</th>
    <th>Company</th>
    <th>Owner</th>
    <th>Vehicle Type</th>
    <th>Model</th>
    <th>Color</th>
    <th>Chassis Number</th>
    <th>Engine Number</th>
   
    <th>Transfer Form</th>
    <th>Requesting Insurance</th>
    <th>Details</th>
  </tr>
 
{this.state.realdata.map((item) =>{ return( <tr>
    {console.log("itemitemitemitem",item)}
    <td>{item.value.vin}</td>
    <td>{item.value.companyName}</td>
      <td>{item.value.owner}</td>
      <td>{item.value.vehicleType}</td>
   
  
  
    <td>{item.value.modal}</td>
    <td>{item.value.color}</td>
    <td>{item.value.chasis_number}</td>
    <td>{item.value.engine_number}</td>
   
     <td><DealerForm item={item} modaltitle="GDT Form" /></td>
     <td><Dealerpolicy item={item} modaltitle="Insurance Form" /></td>
     <td><Link 
    to={{pathname:`specificvechical/${item.value.vin}`} }>
     View</Link></td>
</tr>) })
}
</table>
				</div>
			</div>
     
    
		</div>

   
    
    
    
	</section>
	
 
 
  </>
    )
  }
}
