import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import Newnav from "../component/newnav";
import Check from "../api/genricfunction";
export default class Uploadstudentwallet extends Component {
  state = {
    finallist: "",
    loading: false,
    sucess: false,
    modalRejectinsurance: false,
    formdata: {
      vin: "",
      reject_reason: ""
    }
  };
  render() {
    return (
      <div>
        <Newnav />

        <div className="back2">
          <div className="row" style={{ padding: "0px 0px", margin: "0px" }}>
            <div className="col-lg-2"></div>
            <div className="col-lg-4">
              <h1 className="white">
                Upload your <br /> Identity
              </h1>
            </div>
            <div className="col-lg-6"></div>
            <div className="row" style={{ padding: "0px 0px", margin: "0px" }}>
              <div className="col-lg-2"></div>

              <br />
              <div className="col-lg-6 basetext">
                <br /> Lordshire trust our future with candidates every day. Use
                blockchain to find the ones who get it right. Reputation &
                Decentralized The Decentralized, Reputation ledger allow you to
                find and compare candidates based on how accurate They’ve been
                in academics and past.
              </div>
              <div className="col-lg-4"></div>
            </div>
            <br />
            <div className="col-lg-12 center">
              <br />

              <form className=" validate-form">
                <div
                  className=" validate-input"
                  data-validate="Password is required"
                >
                  <label
                    style={{
                      background: "white",
                      borderRadius: "5px",
                      padding: "5px",
                      fontSize: "16px",
                      fontWeight: "600",
                      pointer: "cursor"
                    }}
                    for="inputfile"
                  >
                    Please Choose Wallet
                  </label>

                  <input
                    style={{ visibility: "hidden" }}
                    accept=".json"
                    id="inputfile"
                    title="Choose a Wallet please"
                    type="file"
                    name="hellp"
                    onClick={async () => {
                      await this.setState({ errormsg: false });
                    }}
                    onChange={event => {
                      this.setState({ finallist: "s" });
                      var file = event.target.files[0];
                      console.log("data which is coming", file);
                      console.log("file type is", typeof file);
                      let reader = new FileReader();
                      console.log("readerreader", reader);
                      reader.readAsBinaryString(file);

                      reader.onload = async () => {
                        await console.log(
                          "first before seeting state",
                          reader.result.replace("ï»¿", "")
                        );

                        await this.setState({
                          jsondata: reader.result.replace("ï»¿", "")
                        });

                        await console.log(
                          " the data that load is ",
                          this.state.jsondata
                        );
                        await console.log(typeof this.state.jsondata);

                        var obj = JSON.parse(this.state.jsondata);

                        console.log(
                          "objobj",
                          obj,
                          "saasa",
                          JSON.parse(this.state.jsondata)
                        );
                        localStorage.setItem(
                          "wallet",
                          JSON.parse(this.state.jsondata)
                        );
                      };
                      reader.onerror = function(error) {
                        console.log("Error: ", error);
                      };
                      // this.setState({mobileNumber: event.target.value});
                    }}
                  />
                </div>

                <div className="container-login100-form-btn">
                  {this.state.finallist != "" ? (
                    <button
                      onClick={async e => {
                        e.preventDefault();

                        Check.setdata(JSON.parse(this.state.jsondata));
                      }}
                      className="login100-form-btn"
                    >
                      Next
                    </button>
                  ) : (
                    <button
                      disabled
                      style={{ opacity: "0.5" }}
                      className="login100-form-btn"
                    >
                      Please Upload Wallet ...
                    </button>
                  )}
                </div>

                <div className="text-center p-t-12">
                  <span className="txt1">
                    {this.state.errormsg && (
                      <p style={{ color: "red", fontWeight: "900" }}>
                        Error is please submit wallet again
                      </p>
                    )}
                  </span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
